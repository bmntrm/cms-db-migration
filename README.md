# cms-db-migration



## Getting started

Proyek "cms-ebook-migration" adalah aplikasi untuk melakukan migrasi basis data dengan menggunakan Sequelize dan PostgreSQL. Proyek ini memungkinkan Anda untuk menjalankan migrasi basis data ke atas (migrate:up) atau ke bawah (migrate:down) dengan mudah.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Instalasi
Sebelum Anda dapat menggunakan proyek ini, pastikan Anda telah menginstal dependensi yang diperlukan. Anda dapat melakukannya dengan menjalankan perintah berikut:

```
npm install
```
Pastikan juga Anda memiliki PostgreSQL yang telah diinstal dan konfigurasi yang sesuai di file config/config.json.

## Penggunaan
Proyek ini dilengkapi dengan dua script yang dapat Anda gunakan:

## Migrate
Untuk menjalankan migrasi basis, jalankan perintah berikut:
```
npm run migrate:up
```
Jika Anda perlu melakukan rollback migrasi, Anda dapat menggunakan perintah berikut:

```
npm run migrate:down
```
Pastikan untuk menjalankan perintah-perintah ini di direktori proyek Anda.

## Generate migration
untuk membuat generate migration baru bisa menggunakan perintah ini

```
npx sequelize-cli migration:generate --name core_name_table
```

## Kontribusi
Kontribusi selalu disambut! Jika Anda ingin berkontribusi pada proyek ini, silakan buat pull request dan kami akan mempertimbangkan kontribusi Anda.

## Lisensi
Proyek ini dilisensikan di bawah lisensi ISC.

