const config = require('./config/config');
const { Sequelize } = require('./models');

const seq = new Sequelize(config.database, config.username, config.password, config);

(async () => {
  try {
    await seq.authenticate();
    console.log('koneksi sukses');
    await seq.close();
  } catch (error) {
    console.log(error.message);
  }
})();
