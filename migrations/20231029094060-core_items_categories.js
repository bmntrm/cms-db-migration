/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_items_categories', {
      item_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      category_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
    });

    // Define primary key constraint
    await queryInterface.addConstraint('core_items_categories', {
      fields: ['item_id', 'category_id'],
      type: 'primary key',
      name: 'core_items_categories_pk',
    });

    // Define foreign key constraint for category_id
    await queryInterface.addConstraint('core_items_categories', {
      fields: ['category_id'],
      type: 'foreign key',
      references: {
        table: 'core_categories',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_items_categories_category_id_fkey',
    });

    // Define foreign key constraint for item_id
    await queryInterface.addConstraint('core_items_categories', {
      fields: ['item_id'],
      type: 'foreign key',
      references: {
        table: 'core_items',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_items_categories_item_id_fkey',
    });

    // Index creation for category_id
    await queryInterface.addIndex('core_items_categories', ['category_id'], {
      name: 'core_items_categories_ix_category_id',
      using: 'BTREE',
    });

    // Index creation for item_id
    await queryInterface.addIndex('core_items_categories', ['item_id'], {
      name: 'core_items_categories_ix_item_id',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_items_categories');
  },
};
