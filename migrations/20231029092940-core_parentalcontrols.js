/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_parentalcontrols', {
      created: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      modified: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(40), // VARCHAR(40)
        allowNull: true,
      },
      description: {
        type: Sequelize.TEXT, // TEXT
        allowNull: true,
      },
      slug: {
        type: Sequelize.STRING(100), // VARCHAR(100)
        allowNull: true,
      },
      meta: {
        type: Sequelize.TEXT, // TEXT
        allowNull: true,
      },
      icon_image_normal: {
        type: Sequelize.STRING(100), // VARCHAR(100)
        allowNull: true,
      },
      icon_image_highres: {
        type: Sequelize.STRING(100), // VARCHAR(100)
        allowNull: true,
      },
      sort_priority: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      parent_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_parentalcontrols');
  },
};
