/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_items_authors', {
      item_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      author_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
    });

    // Define foreign key constraint for author_id
    await queryInterface.addConstraint('core_items_authors', {
      fields: ['author_id'],
      type: 'foreign key',
      references: {
        table: 'core_authors',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_items_authors_author_id_fkey',
    });

    // Define foreign key constraint for item_id
    await queryInterface.addConstraint('core_items_authors', {
      fields: ['item_id'],
      type: 'foreign key',
      references: {
        table: 'core_items',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_items_authors_item_id_fkey',
    });

    // Index creation for author_id
    await queryInterface.addIndex('core_items_authors', ['author_id'], {
      name: 'core_items_authors_ix_author_id',
      using: 'BTREE',
    });

    // Index creation for item_id
    await queryInterface.addIndex('core_items_authors', ['item_id'], {
      name: 'core_items_authors_ix_item_id',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_items_authors');
  },
};
