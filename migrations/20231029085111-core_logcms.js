/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_logcms', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      created: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: Sequelize.fn('NOW'),
      },
      user_id: {
        type: Sequelize.BIGINT,
        allowNull: false,
      },
      data_url: {
        type: Sequelize.STRING(225),
        allowNull: true,
      },
      data_request: {
        type: Sequelize.JSONB,
        allowNull: true,
      },
      data_response: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      host_client: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      method: {
        type: Sequelize.STRING(225),
        allowNull: true,
      },
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_logcms');
  },
};
