/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_brands_defaultauthors', {
      brand_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      author_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
    });

    // Define foreign key constraint for author_id
    await queryInterface.addConstraint('core_brands_defaultauthors', {
      fields: ['author_id'],
      type: 'foreign key',
      references: {
        table: 'core_authors',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_brands_defaultauthors_author_id_fkey',
    });

    // Define foreign key constraint for brand_id
    await queryInterface.addConstraint('core_brands_defaultauthors', {
      fields: ['brand_id'],
      type: 'foreign key',
      references: {
        table: 'core_brands',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_brands_defaultauthors_brand_id_fkey',
    });

    // Index creation
    await queryInterface.addIndex('core_brands_defaultauthors', ['author_id'], {
      name: 'core_brands_defaultauthors_ix_author_id',
      using: 'BTREE',
    });

    await queryInterface.addIndex('core_brands_defaultauthors', ['brand_id'], {
      name: 'core_brands_defaultauthors_ix_brand_id',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_brands_defaultauthors');
  },
};
