/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_vendors', {
      created: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      modified: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      slug: {
        type: Sequelize.TEXT,
        allowNull: true,
        unique: true,
      },
      meta: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      icon_image_normal: {
        type: Sequelize.STRING(100),
        allowNull: true,
      },
      icon_image_highres: {
        type: Sequelize.STRING(100),
        allowNull: true,
      },
      sort_priority: {
        type: Sequelize.SMALLINT,
        allowNull: true,
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      organization_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      vendor_status: {
        type: Sequelize.SMALLINT,
        allowNull: true,
      },
      accounting_identifier: {
        type: Sequelize.STRING(100),
        allowNull: true,
      },
      iso4217_code: {
        type: Sequelize.STRING(3),
        allowNull: true,
      },
    });

    // Index creation
    await queryInterface.addIndex('core_vendors', ['organization_id'], {
      name: 'core_vendors_ix_organization_id',
      using: 'BTREE',
    });
    await queryInterface.addIndex('core_vendors', ['vendor_status'], {
      name: 'core_vendors_ix_vendor_status',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('core_vendors');
  },
};
