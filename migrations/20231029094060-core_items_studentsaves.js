/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_items_studentsaves', {
      item_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      studentsave_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
    });

    // Define foreign key constraint for item_id
    await queryInterface.addConstraint('core_items_studentsaves', {
      fields: ['item_id'],
      type: 'foreign key',
      references: {
        table: 'core_items',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_items_studentsaves_item_id_core_items_fkey',
    });

    // Index creation for item_id
    await queryInterface.addIndex('core_items_studentsaves', ['item_id'], {
      name: 'core_items_studentsaves_ix_item_id',
      using: 'BTREE',
    });

    // Index creation for studentsave_id
    await queryInterface.addIndex('core_items_studentsaves', ['studentsave_id'], {
      name: 'core_items_studentsaves_ix_studentsave_id',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_items_studentsaves');
  },
};
