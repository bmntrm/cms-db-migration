/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_offers_brands', {
      offer_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      brand_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
    });

    // Define primary key constraint
    await queryInterface.addConstraint('core_offers_brands', {
      fields: ['offer_id', 'brand_id'],
      type: 'primary key',
      name: 'core_offers_brands_pkey',
    });

    // Define foreign key constraint for brand_id
    await queryInterface.addConstraint('core_offers_brands', {
      fields: ['brand_id'],
      type: 'foreign key',
      references: {
        table: 'core_brands',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_offers_brands_brand_id_fkey',
    });

    // Define foreign key constraint for offer_id
    await queryInterface.addConstraint('core_offers_brands', {
      fields: ['offer_id'],
      type: 'foreign key',
      references: {
        table: 'core_offers',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_offers_brands_offer_id_fkey',
    });

    // Index creation for brand_id
    await queryInterface.addIndex('core_offers_brands', ['brand_id'], {
      name: 'core_offers_brands_ix_brand_id',
      using: 'BTREE',
    });

    // Index creation for offer_id
    await queryInterface.addIndex('core_offers_brands', ['offer_id'], {
      name: 'core_offers_brands_ix_offer_id',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_offers_brands');
  },
};
