/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_brands', {
      created: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      modified: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.TEXT, // TEXT
        allowNull: true,
      },
      description: {
        type: Sequelize.TEXT, // TEXT
        allowNull: true,
      },
      slug: {
        type: Sequelize.TEXT,
        allowNull: false,
        unique: true,
      },
      meta: {
        type: Sequelize.TEXT, // TEXT
        allowNull: true,
      },
      icon_image_normal: {
        type: Sequelize.STRING(100), // VARCHAR(100)
        allowNull: true,
      },
      icon_image_highres: {
        type: Sequelize.STRING(100), // VARCHAR(100)
        allowNull: true,
      },
      sort_priority: {
        type: Sequelize.SMALLINT,
        allowNull: true,
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      vendor_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      daily_release_quota: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      default_parentalcontrol_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      default_countries: {
        type: Sequelize.ARRAY(Sequelize.STRING), // ARRAY of VARCHAR
        allowNull: false,
        defaultValue: [],
      },
      default_languages: {
        type: Sequelize.ARRAY(Sequelize.STRING), // ARRAY of VARCHAR
        allowNull: false,
        defaultValue: [],
      },
      default_item_type: {
        type: Sequelize.STRING, // VARCHAR
        allowNull: true,
      },
      primary_category_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      brand_code: {
        type: Sequelize.STRING(50), // VARCHAR(50)
        allowNull: true,
      },
      default_item_schedule: {
        type: Sequelize.STRING(20), // VARCHAR(20)
        allowNull: true,
      },
      release_period: {
        type: Sequelize.STRING, // VARCHAR
        allowNull: true,
      },
    });

    // Index creation
    await queryInterface.addIndex('core_brands', ['vendor_id'], {
      name: 'core_brands_ix_vendor_id',
      using: 'BTREE',
    });

    // Foreign key constraints
    await queryInterface.addConstraint('core_brands', {
      name: 'core_brands_primary_category_id_fkey',
      fields: ['primary_category_id'],
      type: 'foreign key',
      references: {
        table: 'core_categories',
        field: 'id',
      },
      onDelete: 'SET NULL',
      onUpdate: 'CASCADE',
    });

    await queryInterface.addConstraint('core_brands', {
      name: 'core_brands_vendor_id_fkey',
      fields: ['vendor_id'],
      type: 'foreign key',
      references: {
        table: 'core_vendors',
        field: 'id',
      },
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_brands');
  },
};
