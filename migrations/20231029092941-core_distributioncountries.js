/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_distributioncountries', {
      created: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      modified: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(250), // VARCHAR(250)
        allowNull: true,
      },
      group_type: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      countries: {
        type: Sequelize.ARRAY(Sequelize.STRING), // ARRAY of VARCHAR
        allowNull: true,
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      vendor_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_distributioncountries');
  },
};
