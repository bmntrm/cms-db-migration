/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_categories', {
      created: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      modified: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(40), // VARCHAR(40)
        allowNull: false,
      },
      description: {
        type: Sequelize.TEXT, // TEXT
        allowNull: true,
      },
      slug: {
        type: Sequelize.STRING(100), // VARCHAR(100)
        allowNull: false,
      },
      meta: {
        type: Sequelize.TEXT, // TEXT
        allowNull: true,
      },
      icon_image_normal: {
        type: Sequelize.STRING(100), // VARCHAR(100)
        allowNull: true,
      },
      icon_image_highres: {
        type: Sequelize.STRING(100), // VARCHAR(100)
        allowNull: true,
      },
      sort_priority: {
        type: Sequelize.SMALLINT,
        allowNull: false,
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      parent_category_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      countries: {
        type: Sequelize.ARRAY(Sequelize.STRING), // ARRAY of VARCHAR
        allowNull: false,
        defaultValue: [],
      },
      item_type: {
        type: Sequelize.STRING, // VARCHAR
        allowNull: false,
      },
      thumb_image_normal: {
        type: Sequelize.STRING(250), // VARCHAR(250)
        allowNull: true,
      },
    });

    // Foreign key constraint
    await queryInterface.addConstraint('core_categories', {
      name: 'core_categories_parent_category_id_fkey',
      fields: ['parent_category_id'],
      type: 'foreign key',
      references: {
        table: 'core_categories',
        field: 'id',
      },
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_categories');
  },
};
