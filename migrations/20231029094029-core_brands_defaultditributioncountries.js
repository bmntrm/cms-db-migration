/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_brands_defaultditributioncountries', {
      brand_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      distribution_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
    });

    // Define foreign key constraint for brand_id
    await queryInterface.addConstraint('core_brands_defaultditributioncountries', {
      fields: ['brand_id'],
      type: 'foreign key',
      references: {
        table: 'core_brands',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_brands_defaultditributioncountries_brand_id_fkey',
    });

    // Define foreign key constraint for distribution_id
    await queryInterface.addConstraint('core_brands_defaultditributioncountries', {
      fields: ['distribution_id'],
      type: 'foreign key',
      references: {
        table: 'core_distributioncountries',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_brands_defaultditributioncountries_distribution_id_fkey',
    });

    // Index creation
    await queryInterface.addIndex('core_brands_defaultditributioncountries', ['brand_id'], {
      name: 'core_brands_defaultditributioncountries_ix_brand_id',
      using: 'BTREE',
    });

    await queryInterface.addIndex('core_brands_defaultditributioncountries', ['distribution_id'], {
      name: 'core_brands_defaultditributioncountries_ix_distribution_id',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_brands_defaultditributioncountries');
  },
};
