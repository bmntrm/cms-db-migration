/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_offers_items', {
      offer_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      item_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
    });

    // Define primary key constraint
    await queryInterface.addConstraint('core_offers_items', {
      fields: ['item_id', 'offer_id'],
      type: 'primary key',
      name: 'core_offers_items_pkey',
    });

    // Define foreign key constraint for item_id
    await queryInterface.addConstraint('core_offers_items', {
      fields: ['item_id'],
      type: 'foreign key',
      references: {
        table: 'core_items',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_offers_items_item_id_fkey',
    });

    // Define foreign key constraint for offer_id
    await queryInterface.addConstraint('core_offers_items', {
      fields: ['offer_id'],
      type: 'foreign key',
      references: {
        table: 'core_offers',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_offers_items_offer_id_fkey',
    });

    // Index creation for item_id
    await queryInterface.addIndex('core_offers_items', ['item_id'], {
      name: 'core_offers_items_ix_item_id',
      using: 'BTREE',
    });

    // Index creation for offer_id
    await queryInterface.addIndex('core_offers_items', ['offer_id'], {
      name: 'core_offers_items_ix_offer_id',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_offers_items');
  },
};
