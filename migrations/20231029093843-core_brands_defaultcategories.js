/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_brands_defaultcategories', {
      brand_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      category_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
    });

    // Define foreign key constraint for brand_id
    await queryInterface.addConstraint('core_brands_defaultcategories', {
      fields: ['brand_id'],
      type: 'foreign key',
      references: {
        table: 'core_brands',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_brands_defaultcategories_brand_id_fkey',
    });

    // Define foreign key constraint for category_id
    await queryInterface.addConstraint('core_brands_defaultcategories', {
      fields: ['category_id'],
      type: 'foreign key',
      references: {
        table: 'core_categories',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_brands_defaultcategories_category_id_fkey',
    });

    // Index creation
    await queryInterface.addIndex('core_brands_defaultcategories', ['brand_id'], {
      name: 'core_brands_defaultcategories_ix_brand_id',
      using: 'BTREE',
    });

    await queryInterface.addIndex('core_brands_defaultcategories', ['category_id'], {
      name: 'core_brands_defaultcategories_ix_category_id',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_brands_defaultcategories');
  },
};
