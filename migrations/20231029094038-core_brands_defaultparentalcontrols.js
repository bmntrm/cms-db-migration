/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_brands_defaultparentalcontrols', {
      brand_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      parental_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
    });

    // Define foreign key constraint for brand_id
    await queryInterface.addConstraint('core_brands_defaultparentalcontrols', {
      fields: ['brand_id'],
      type: 'foreign key',
      references: {
        table: 'core_brands',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_brands_defaultparentalcontrols_brand_id_fkey',
    });

    // Define foreign key constraint for parental_id
    await queryInterface.addConstraint('core_brands_defaultparentalcontrols', {
      fields: ['parental_id'],
      type: 'foreign key',
      references: {
        table: 'core_parentalcontrols',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_brands_defaultparentalcontrols_parental_id_fkey',
    });

    // Index creation
    await queryInterface.addIndex('core_brands_defaultparentalcontrols', ['brand_id'], {
      name: 'core_brands_defaultparentalcontrols_ix_brand_id',
      using: 'BTREE',
    });

    await queryInterface.addIndex('core_brands_defaultparentalcontrols', ['parental_id'], {
      name: 'core_brands_defaultparentalcontrols_ix_parental_id',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_brands_defaultparentalcontrols');
  },
};
