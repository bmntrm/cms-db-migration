/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_items_categories_ax', {
      item_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      category_ax_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      is_retail: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      category_ax: {
        type: Sequelize.STRING(100),
        allowNull: true,
      },
    });

    // Define foreign key constraint for category_ax_id
    await queryInterface.addConstraint('core_items_categories_ax', {
      fields: ['category_ax_id'],
      type: 'foreign key',
      references: {
        table: 'core_categories_ax',
        field: 'id', // Change this to the actual field name in core_categories_ax table
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_items_categories_ax_category_ax_id_fkey',
    });

    // Index creation for category_ax_id
    await queryInterface.addIndex('core_items_categories_ax', ['category_ax_id'], {
      name: 'core_items_categories_ax_ix_category_ax_id',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_items_categories_ax');
  },
};
