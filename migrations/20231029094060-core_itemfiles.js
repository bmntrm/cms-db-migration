/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_itemfiles', {
      created: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      modified: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      item_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      file_type: {
        type: Sequelize.SMALLINT,
        allowNull: false,
      },
      file_name: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      file_order: {
        type: Sequelize.SMALLINT,
        allowNull: false,
      },
      file_status: {
        type: Sequelize.SMALLINT,
        allowNull: false,
      },
      file_version: {
        type: Sequelize.STRING(250), // VARCHAR(250)
        allowNull: true,
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      key: {
        type: Sequelize.STRING(250), // VARCHAR(250)
        allowNull: true,
      },
      md5_checksum: {
        type: Sequelize.STRING(250), // VARCHAR(250)
        allowNull: true,
      },
      page_view: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
    });

    // Define foreign key constraint for item_id
    await queryInterface.addConstraint('core_itemfiles', {
      fields: ['item_id'],
      type: 'foreign key',
      references: {
        table: 'core_items',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_itemfiles_item_id_fkey',
    });

    // Index creation for file_order
    await queryInterface.addIndex('core_itemfiles', ['file_order'], {
      name: 'core_itemfiles_ix_file_order',
      using: 'BTREE',
    });

    // Index creation for item_id
    await queryInterface.addIndex('core_itemfiles', ['item_id'], {
      name: 'core_itemfiles_ix_item_id',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_itemfiles');
  },
};
