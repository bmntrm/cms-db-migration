/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_offers', {
      created: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      modified: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(150), // VARCHAR(150)
        allowNull: true,
      },
      offer_status: {
        type: Sequelize.SMALLINT,
        allowNull: false,
      },
      sort_priority: {
        type: Sequelize.SMALLINT,
        allowNull: false,
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      offer_type_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      exclusive_clients: {
        type: Sequelize.ARRAY(Sequelize.STRING), // ARRAY of VARCHAR
        allowNull: true,
      },
      is_free: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      offer_code: {
        type: Sequelize.STRING(250), // VARCHAR(250)
        allowNull: true,
      },
      item_code: {
        type: Sequelize.STRING(250), // VARCHAR(250)
        allowNull: true,
      },
      price_usd: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: false,
      },
      price_idr: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: false,
      },
      price_point: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      discount_id: {
        type: Sequelize.ARRAY(Sequelize.STRING), // ARRAY of VARCHAR
        allowNull: true,
      },
      discount_tag: {
        type: Sequelize.STRING(150), // VARCHAR(150)
        allowNull: true,
      },
      discount_name: {
        type: Sequelize.STRING(150), // VARCHAR(150)
        allowNull: true,
      },
      discount_price_usd: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: true,
      },
      discount_price_idr: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: true,
      },
      discount_price_point: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      is_discount: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      image_highres: {
        type: Sequelize.STRING(150), // VARCHAR(150)
        allowNull: true,
      },
      image_normal: {
        type: Sequelize.STRING(150), // VARCHAR(150)
        allowNull: true,
      },
      vendor_price_usd: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: true,
      },
      vendor_price_idr: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: true,
      },
      vendor_price_point: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      long_name: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      colors: {
        type: Sequelize.JSON, // JSON
        allowNull: true,
      },
      image_banner: {
        type: Sequelize.JSON, // JSON
        allowNull: true,
      },
    });

    // Define foreign key constraint for offer_type_id
    await queryInterface.addConstraint('core_offers', {
      fields: ['offer_type_id'],
      type: 'foreign key',
      references: {
        table: 'core_offertypes',
        field: 'id',
      },
      name: 'core_offers_offer_type_id_fkey',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_offers');
  },
};
