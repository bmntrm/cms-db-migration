/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_authors', {
      created: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      modified: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      sort_priority: {
        type: Sequelize.SMALLINT,
        allowNull: true,
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      slug: {
        type: Sequelize.TEXT,
        allowNull: false,
        unique: true,
      },
      first_name: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      last_name: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      title: {
        type: Sequelize.STRING(5), // VARCHAR(5)
        allowNull: true,
      },
      academic_title: {
        type: Sequelize.STRING(20), // VARCHAR(20)
        allowNull: true,
      },
      birthdate: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      meta: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      profile_pic_url: {
        type: Sequelize.STRING(255), // VARCHAR(255)
        allowNull: true,
      },
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_authors');
  },
};
