/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_items', {
      created: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      modified: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(250), // VARCHAR(250)
        allowNull: false,
      },
      edition_code: {
        type: Sequelize.STRING(250), // VARCHAR(250)
        allowNull: false,
      },
      sort_priority: {
        type: Sequelize.SMALLINT,
        allowNull: false,
      },
      release_date: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      is_featured: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      is_extra: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      parentalcontrol_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      brand_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      display_until: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      item_distribution_country_group_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      release_schedule: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      countries: {
        type: Sequelize.ARRAY(Sequelize.STRING), // ARRAY of VARCHAR
        allowNull: false,
        defaultValue: [],
      },
      languages: {
        type: Sequelize.ARRAY(Sequelize.STRING), // ARRAY of VARCHAR
        allowNull: false,
        defaultValue: [],
      },
      item_type: {
        type: Sequelize.ENUM('book', 'magazine', 'newspaper', 'audio book'), // Update with the actual ENUM values
        allowNull: true,
      },
      content_type: {
        type: Sequelize.ENUM('pdf', 'epub', 'mp3'), // Update with the actual ENUM values
        allowNull: true,
      },
      item_status: {
        type: Sequelize.ENUM('new', 'ready for upload', 'uploaded', 'waiting for review', 'rejected, approved', 'prepare for consume', 'ready for consume', 'not for consume', 'in review', 'mcgrawhill content'), // Update with the actual ENUM values
        allowNull: true,
      },
      slug: {
        type: Sequelize.TEXT,
        allowNull: true,
        unique: true,
      },
      thumb_image_normal: {
        type: Sequelize.STRING(100), // VARCHAR(100)
        allowNull: true,
      },
      thumb_image_highres: {
        type: Sequelize.STRING(100), // VARCHAR(100)
        allowNull: true,
      },
      issue_number: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      extra_items: {
        type: Sequelize.ARRAY(Sequelize.STRING), // ARRAY of VARCHAR
        allowNull: true,
      },
      subs_weight: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      image_normal: {
        type: Sequelize.STRING(150), // VARCHAR(150)
        allowNull: true,
      },
      image_highres: {
        type: Sequelize.STRING(150), // VARCHAR(150)
        allowNull: true,
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      meta: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      filenames: {
        type: Sequelize.ARRAY(Sequelize.STRING), // ARRAY of VARCHAR
        allowNull: true,
      },
      previews: {
        type: Sequelize.ARRAY(Sequelize.STRING), // ARRAY of VARCHAR
        allowNull: true,
      },
      reading_direction: {
        type: Sequelize.ENUM('left-to-right', 'right-to-left'), // Update with the actual ENUM values
        allowNull: true,
      },
      gtin13: {
        type: Sequelize.STRING(25), // VARCHAR(25)
        allowNull: true,
      },
      gtin14: {
        type: Sequelize.STRING(27), // VARCHAR(27)
        allowNull: true,
      },
      gtin8: {
        type: Sequelize.STRING(15), // VARCHAR(15)
        allowNull: true,
      },
      revision_number: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      printed_price: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: true,
      },
      printed_currency_code: {
        type: Sequelize.STRING(3), // VARCHAR(3)
        allowNull: true,
      },
      page_count: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      vendor_product_id_print: {
        type: Sequelize.STRING(15), // VARCHAR(15)
        allowNull: true,
      },
      file_size: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      is_internal_content: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      parent_item_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
    });

    // Define foreign key constraint for brand_id
    await queryInterface.addConstraint('core_items', {
      fields: ['brand_id'],
      type: 'foreign key',
      references: {
        table: 'core_brands',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_items_brand_id_fkey',
    });

    // Define foreign key constraint for parentalcontrol_id
    await queryInterface.addConstraint('core_items', {
      fields: ['parentalcontrol_id'],
      type: 'foreign key',
      references: {
        table: 'core_parentalcontrols',
        field: 'id',
      },
      onDelete: 'CASCADE', // You can choose the appropriate action
      onUpdate: 'CASCADE', // based on your requirements
      name: 'core_items_parentalcontrol_id_fkey',
    });

    // Index creation for brand_id
    await queryInterface.addIndex('core_items', ['brand_id'], {
      name: 'core_items_ix_brand_id',
      using: 'BTREE',
    });

    // Index creation for content_type
    await queryInterface.addIndex('core_items', ['content_type'], {
      name: 'core_items_ix_content_type',
      using: 'BTREE',
    });

    // Index creation for edition_code
    await queryInterface.addIndex('core_items', ['edition_code'], {
      name: 'core_items_ix_edition_code',
      using: 'BTREE',
    });

    // Index creation for is_active
    await queryInterface.addIndex('core_items', ['is_active'], {
      name: 'core_items_ix_is_active',
      using: 'BTREE',
    });

    // Index creation for item_status
    await queryInterface.addIndex('core_items', ['item_status'], {
      name: 'core_items_ix_item_status',
      using: 'BTREE',
    });

    // Index creation for item_type
    await queryInterface.addIndex('core_items', ['item_type'], {
      name: 'core_items_ix_item_type',
      using: 'BTREE',
    });

    // Index creation for parentalcontrol_id
    await queryInterface.addIndex('core_items', ['parentalcontrol_id'], {
      name: 'core_items_ix_parentalcontrol_id',
      using: 'BTREE',
    });

    // Index creation for release_date
    await queryInterface.addIndex('core_items', ['release_date'], {
      name: 'core_items_ix_release_date',
      using: 'BTREE',
    });

    // Index creation for release_schedule
    await queryInterface.addIndex('core_items', ['release_schedule'], {
      name: 'core_items_ix_release_schedule',
      using: 'BTREE',
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_items');

    await queryInterface.sequelize.query('DROP TYPE public.enum_core_items_content_type');
    await queryInterface.sequelize.query('DROP TYPE public.enum_core_items_item_type');
    await queryInterface.sequelize.query('DROP TYPE public.enum_core_items_item_status');
    await queryInterface.sequelize.query('DROP TYPE public.enum_core_items_reading_direction');
  },
};
