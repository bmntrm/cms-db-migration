/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('core_offertypes', {
      created: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      modified: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(150), // VARCHAR(150)
        allowNull: true,
      },
      description: {
        type: Sequelize.TEXT, // TEXT
        allowNull: true,
      },
      slug: {
        type: Sequelize.STRING(100), // VARCHAR(100)
        allowNull: false,
      },
      meta: {
        type: Sequelize.TEXT, // TEXT
        allowNull: true,
      },
      sort_priority: {
        type: Sequelize.SMALLINT,
        allowNull: true,
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
    });
  },

  down: async (queryInterface) => {
    // Drop the table
    await queryInterface.dropTable('core_offertypes');
  },
};
